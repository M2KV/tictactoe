import { useState } from "react";

const row = 3, col = 3;

function Square({ value, onSquareClick, isWinner }) {
    return (
        <button className={'square' + ((isWinner) ? ' win' : '')} onClick={onSquareClick}>
            {value}
        </button>
    );
}

function Board({ xIsNext, squares, onPlay }) {
	function handleClick(i) {
        if (calculateWinner(squares).value || squares[i]) {
            return;
        }
        const nextSquares = squares.slice();
        if (xIsNext) {
            nextSquares[i] = "X";
        } else {
            nextSquares[i] = "O";
        }

        onPlay(nextSquares, {
			value: nextSquares[i],
			position: `(${((i - (i % col)) / row)}, ${i % col})`
		});
    }

    function createRow(col, i) {
        return Array(col)
            .fill(null)
            .map((_, j) => (
                <Square
                    key={i * row + j}
                    value={squares[i * row + j]}
                    onSquareClick={() => handleClick(i * row + j)}
					isWinner={calculateWinner(squares).indexes.includes(i * row + j)}
                />
            ));
    }

    function createBoard(row, col) {
        return Array(row)
            .fill(null)
            .map((_, i) => (
                <div key={i} className="board-row">
                    {createRow(col, i)}
                </div>
            ));
    }

    const winner = calculateWinner(squares).value;
    let status;
    if (winner) {
        status = "Winner: " + winner;
    } else {
		const currentLength = squares.filter(el => el != null).length - 1;
		if (currentLength === (row * col) - 1) {
			status = "Draw: X, O";
		}
		else 
        	status = "Next player: " + (xIsNext ? "X" : "O");
    }

    return (
        <>
            <div className="status">{status}</div>
            {createBoard(row, col)}
        </>
    );
}

export default function Game() {
    const [history, setHistory] = useState([Array(row*col).fill(null)]);
    const [currentMove, setCurrentMove] = useState(0);
	const [sort, setSort] = useState(false);
    const xIsNext = currentMove % 2 === 0;
    const currentSquares = history[currentMove];
	const [historyPosition, setHistoryPosition] = useState([]);

    function handlePlay(nextSquares, historyPosObject) {
        const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
        setHistory(nextHistory);
        setCurrentMove(nextHistory.length - 1);

		const historyPosList = [...historyPosition];
		historyPosList[nextHistory.length - 1] = historyPosObject;
		setHistoryPosition(historyPosList);
    }

    function jumpTo(nextMove) {
        setCurrentMove(nextMove);
    }

	function Sort() {
		setSort(prev => !prev);
	}

    const moves = history.map((squares, move) => {

        let description;
        if (move > 0) {
            description = `Go to move #${move} with ${historyPosition[move].value} move at ${historyPosition[move].position}`;
        } else {
            description = "Go to game start";
        }
        return (
            <li key={move}>
                {move === currentMove && move > 0 ? (
                    `You are at move #${move} with ${historyPosition[move].value} move at ${historyPosition[move].position}`
                ) : (
                    <button class="history_btn" onClick={() => jumpTo(move)}>
                        {description}
                    </button>
                )}
            </li>
        );
    });

    return (
        <div className="game">
            <div className="game-board">
                <Board
                    xIsNext={xIsNext}
                    squares={currentSquares}
                    onPlay={handlePlay}
                />
            </div>
            <div className="game-info">
				<button class="sort-btn" onClick={Sort}>{ !sort ? 'Descending' : 'Ascending' }</button>
                <ol>{!sort ? moves : moves.reverse()}</ol>
            </div>
        </div>
    );
}

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (const element of lines) {
        const [a, b, c] = element;
        if (
            squares[a] &&
            squares[a] === squares[b] &&
            squares[a] === squares[c]
        ) {
            return { indexes: element, value: squares[a] };
		}
    }
    return { value: null, indexes: [] };
}

